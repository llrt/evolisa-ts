const path = require('path');

module.exports = {
    entry: "./app.ts",
    // devtool: "source-map",
    resolve: {
        extensions: [".ts",".js"]
    },
    module: {
        loaders: [
            {test: /\.ts$/, loader: "ts-loader", exclude: /node_modules/}
        ]
    },
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, 'dist')
    }
}