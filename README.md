# evolisa-ts

A Typescript implementation of [Roger Alsing's Evolisa idea](https://rogerjohansson.blog/2008/12/07/genetic-programming-evolution-of-mona-lisa/), created for fun and Typescript lang learning.

Also inspired in [oysteinkrog's refactored C# version](https://github.com/oysteinkrog/EvoLisa) and [willcodefortea's js (with webworkers!) version](https://github.com/willcodefortea/evo-lisa).